package ticketPackage;
import java.util.regex.*;
public class TiValidate {
	
	TiCon connection = new TiCon();
	TiValidate(){
		
	}
	public boolean validate(String aString, String pattern){
		return Pattern.matches(pattern, aString);
	}
	//---------------------- Main Menu
	public boolean mainMenuValidate(String aString){
		return Pattern.matches("[1-3]", aString);
	}
	//---------------------- Regular User Menu
	public boolean regularMenuValidate(String aString){
		return Pattern.matches("[1-7]", aString);
	}
	//---------------------- Admin Menu
	public boolean adminMenuValidate(String aString){
		return Pattern.matches("[1-7]", aString);
	}
	public boolean sqlValidate(String query){ // use SqlQuery class for check instead // create array that gets a size from the query, check if size == 0
		boolean aBoolean = false;
		connection.connectToDatabase("flight");
		try{
			connection.myResultSet = connection.myStatement.executeQuery(query);
			if(connection.myResultSet.next()){
				aBoolean = true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		connection.closeConnection();
		return aBoolean;
	}
}
