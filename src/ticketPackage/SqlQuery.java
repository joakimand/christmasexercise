package ticketPackage;
import java.sql.*;

public class SqlQuery {
	private Connection myConnection;
	private Statement myStatement;
	private ResultSet myResultSet;
	String myUrl = null, myUser = null, myPassword = null, myDatabase = null;
	SqlQuery(){
		myUrl = "jdbc:mysql://localhost:3306/"; //DEFAULT VALUES
		myUser = "root"; 
		myPassword = "";
	}
	/*public void setConnection(String url, String user, String password){ // Use without a specified database
		this.myUrl = url;
		this.myUser = user; 
		this.myPassword = password;
	}
	public void setConnection(String url, String user, String password, String database){ // Use with a specified database
		this.myUrl = url;
		this.myUser = user;
		this.myPassword = password;
		this.myDatabase = database;
	}*/
	//------------------------------------
	private void closeConnection(){
		try{
			if(myConnection != null){ // is there a connection?
				myConnection.close(); // close the connection
			}
		}
		catch(Exception e){

		}
	}
	//------------------------------------
		private void openConnection(String url, String user, String password){
			this.myUser = user;
			this.myUrl = url;
			this.myPassword = password;
			try{
				myConnection = DriverManager.getConnection(myUrl, myUser, myPassword); // connect using given database name
				myStatement = myConnection.createStatement();
			}
			catch(Exception e){
				e.printStackTrace();
			}
		}
	//------------------------------------
	private void openConnection(String url, String user, String password, String database){
		this.myUser = user;
		this.myUrl = url;
		this.myPassword = password;
		this.myDatabase = database;
		try{
			myConnection = DriverManager.getConnection(myUrl + database, myUser, myPassword); // connect using given database name
			myStatement = myConnection.createStatement();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	//------------------------------------------------------------------------------------- InsertData
	public void insertData(String table, String[] column, String[] value){
		openConnection(myUrl, myUser, myPassword, "flight"); // connect to the database
		String query = "INSERT IGNORE INTO `"+table+"` (`"+column[0]+"`"; // write the base start of the query
		for(int i = 1; i < column.length; i++){ // add number of columns+1's names to the query
			query += ", `"+column[i]+"`";
		}
		if(!(value[0].contains("(SELECT"))){ // regular data types (first value ONLY!)
			query += ") VALUES ('"+value[0]+"'"; // add the second half of the base of the query
		}
		else{ // if you for some reason would like to use a select (first value ONLY!) 
			query += ") VALUES ("+value[0]+""; // add the second half of the base of the query
		}
		for(int i = 1; i < value.length; i++){ // add number of values with the values you want from the array to the query
			if(!(value[i].contains("(SELECT"))){ // regular data types
				query += ", '"+value[i]+"'";
			}
			else{ // if you for some reason would like to use a select 
				query += ", "+value[i]+"";
			}
		}
		query += ");"; // add the last closure part of the query
		//System.out.println(query); // print query for bug checking
		try{
			myStatement.executeUpdate(query); // execute the query
		}
		catch(Exception e){
			e.printStackTrace();
		}
		closeConnection();
	}
	//-------------------------------------------------------------------------------- /InsertData*/
	//------------------------------------------------------------------------------------- RetriveColumn
	public String[] retrieveColumn(String[] table, String[] column){
		openConnection(myUrl, myUser, myPassword, "flight");
		String query = "SELECT "+column[0]+"";
		String[] aStringArray;
		int columnCount = 0;
		int aCounter = 0;
		int columnsXrows = 0;
		for(int i = 1; i < column.length; i++){
			query += ",`" + column[i] + "`";
		}
		query += " FROM `"+table[0]+"`";
		for(int i = 1; i < column.length; i++){
			query += ",`" + table[i] + "`";
		}
		query += " ;";
		try {
			//System.out.println(query);
			myResultSet = myStatement.executeQuery(query);
			ResultSetMetaData rSMetadata = myResultSet.getMetaData();
			columnCount = rSMetadata.getColumnCount();
			myResultSet.last();
			columnsXrows = columnCount * myResultSet.getRow();
			//System.out.println(columnsXrows); // print size (number of columns times number of rows)
			myResultSet.beforeFirst();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		aStringArray = new String[columnsXrows]; // Need to add length here to be able to return the array //+rowCount
		try{
			while(myResultSet.next()){
				for(int i = 0; i < columnCount; i++){
					aStringArray[aCounter] = myResultSet.getString(i+1); // remove aCounter ?
					aCounter++;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		closeConnection();
		return aStringArray;
	}
	//-------------------------------------------------------------------------------------
	public String[] retrieveColumn(String[] table, String[] column, String[] where, String[] data){
		openConnection(myUrl, myUser, myPassword, "flight");
		String query = "SELECT "+column[0]+"";
		String[] aStringArray;
		int columnCount = 0;
		int aCounter = 0;
		int columnsXrows = 0;
		for(int i = 1; i < column.length; i++){
			query += ",`" + column[i] + "`";
		}
		query += " FROM `"+table[0]+"`";
		for(int i = 1; i < column.length; i++){
			query += ",`" + table[i] + "`";
		}
		query += " WHERE `"+where[0]+"` LIKE '"+data[0]+"'";
		for(int i = 1; i < data.length; i++){
			query += "AND `"+where[i]+"` LIKE '"+data[i]+"'";
		}
		query += ";";
		try {
			//System.out.println(query);
			myResultSet = myStatement.executeQuery(query);
			ResultSetMetaData rSMetadata = myResultSet.getMetaData();
			columnCount = rSMetadata.getColumnCount();
			myResultSet.last();
			columnsXrows = columnCount * myResultSet.getRow();
			//System.out.println(columnsXrows); // print size (number of columns times number of rows)
			myResultSet.beforeFirst();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		aStringArray = new String[columnsXrows]; // Need to add length here to be able to return the array //+rowCount
		try{
			while(myResultSet.next()){
				for(int i = 0; i < columnCount; i++){
					aStringArray[aCounter] = myResultSet.getString(i+1);
					aCounter++;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		closeConnection();
		return aStringArray;
	}
	//------------------------------------------------------------------------------------- // UPDATE
	public String[] retrieveColumn(String[] table, String[] column, String[] where, String[] data, String[] selectColumn){ 
		openConnection(myUrl, myUser, myPassword, "flight");
		String query = "SELECT "+column[0]+"";
		String[] aStringArray;
		int columnCount = 0;
		int aCounter = 0;
		int columnsXrows = 0;
		for(int i = 1; i < column.length; i++){
			query += ",`" + column[i] + "`";
		}
		query += " FROM `"+table[0]+"` WHERE `"+where[0]+"` LIKE ";
		for(int i = 0; i < selectColumn.length; i++){
			query += "'(SELECT `"+selectColumn[i]+"` FROM `"+table[i+1]+"` WHERE `"+where[i+1]+"` LIKE ";
		}
		query += data;
		for(int i = 0; i < selectColumn.length; i++){
			query += ")'";
		}
		// -- add AND function --
		try {
			//System.out.println(query); // comment this
			myResultSet = myStatement.executeQuery(query);
			ResultSetMetaData rSMetadata = myResultSet.getMetaData();
			columnCount = rSMetadata.getColumnCount();
			myResultSet.last();
			columnsXrows = columnCount * myResultSet.getRow();
			//System.out.println(columnsXrows); // print size (number of columns times number of rows)
			myResultSet.beforeFirst();
		}catch (SQLException e) {
			e.printStackTrace();
		}
		aStringArray = new String[columnsXrows]; // Need to add length here to be able to return the array
		try{
			while(myResultSet.next()){
				aCounter++;
				for(int i = 0; i < columnCount; i++){
					aStringArray[aCounter] = myResultSet.getString(i+1);
					aCounter++;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		closeConnection();
		return aStringArray;
	}
	//------------------------------------------------------------------------------------- /RetriveColumn*/
	public void createDatabase(String database){
		openConnection(myUrl, myUser, myPassword);
		String query = "CREATE DATABASE `"+database+"`;";
		try{
			myStatement.executeQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	//-------------------------------------------------------------------------------------
	/*public void createTable(String table, String[] column, String[] datatype, String keys){
		openConnection(myUrl, myUser, myPassword, myDatabase);
		String query = "CREATE TABLE `"+table+"` (`` "+datatype+";";
		try{
			myStatement.executeQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	//-------------------------------------------------------------------------------------
	/*public void createTable(String table, String[] column, String[] foreignkey){ // add / change
		openConnection(myUrl, myUser, myPassword, myDatabase);
		String query = "CREATE TABLE `"+table+"`;";
		try{
			myStatement.executeQuery(query); // CONSTRAINT `us_em` FOREIGN KEY `fk_cu`(`email`) REFERENCES `flight`.`customer`(`email`))
		}catch(Exception e){
			e.printStackTrace();
		}
	}*/
	//------------------------------------------------------------------------------------- FindUser
	protected boolean findUser(String email, String password){
		boolean aBoolean = false;
		openConnection(myUrl, myUser, myPassword, "flight");
		try{
			myResultSet = myStatement.executeQuery("SELECT `email` FROM `flight`.`user` WHERE `email` LIKE \""+email+"\" AND `password` LIKE \""+password+"\"");
			if(myResultSet.next()){
				aBoolean = true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		closeConnection();
		return aBoolean;
	}
	protected boolean findUser(String email, String password, String arg){
		boolean aBoolean = false;
		openConnection(myUrl, myUser, myPassword, "flight");
		try{
			myResultSet = myStatement.executeQuery("SELECT `email` FROM `flight`.`user` WHERE `"+arg+"` LIKE (SELECT `"+arg+"` FROM `user` WHERE `email` LIKE \""+email+"\" AND `password` LIKE \""+password+"\")");
			if(myResultSet.next()){
				aBoolean = true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		closeConnection();
		return aBoolean;
	}
	//------------------------------------------------------------------------------------- /FindUser
} //------------------------------------------------------------------------------------- End of class