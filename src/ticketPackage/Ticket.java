package ticketPackage;

public class Ticket {
	private String ssn = null, name = null, surName = null, cityArrival = null, cityDeparture = null,
			flightCompany = null, plane = null, date = null;

	Ticket(String[] ticket){
		this.ssn = ticket[0];
		this.name = ticket[1];
		this.surName = ticket[2];
		this.cityArrival = ticket[3];
		this.cityDeparture = ticket[4];
		this.flightCompany = ticket[5];
		this.plane = ticket[6];
		this.date = ticket[7];
	}
	public String getSsn(){
		return this.ssn;
	}
	public String getName(){
		return this.name;
	}
	public String getSurname(){
		return this.surName;
	}
	public String getCityArrival(){
		return this.cityArrival;
	}
	public String getCityDeparture(){
		return this.cityDeparture;
	}
	public String getFlightCompany(){
		return this.flightCompany;
	}
	public String getPlane(){
		return this.plane;
	}
	public String getDate(){
		return this.date;
	}
	public String getToString(){
		return this.ssn + " " + this.name + " " + this.surName + " " + this.cityArrival + " " + this.cityDeparture + " " + this.flightCompany + " " + this.plane + " " + this.date;
	}
}
