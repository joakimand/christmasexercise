package ticketPackage;
import java.sql.*;

// connects to the given database

public class TiCon {
	// -------------------- local class variables
	private String myUrl;
	private String myUser;
	private String myPassword;
	//private String database;
	// -------------------- local objects
	protected Connection myConnection;
	protected Statement myStatement;
	protected ResultSet myResultSet;
	DatabaseMetaData metadata;
	
	public TiCon(){
		myUrl = "jdbc:mysql://localhost:3306/";
		myUser = "root";
		myPassword = "";
		try {
			myConnection = DriverManager.getConnection(myUrl, myUser, myPassword);
			myStatement = myConnection.createStatement();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
		//-----------------------------------------------------------
	protected void closeConnection(){
		try{
			if(myConnection != null){ // is there a connection?
				myConnection.close(); // close the connection
			}
		}
		catch(Exception e){
			
		}
	}
	protected void connectToDatabase(String database){
		try{
			myConnection = DriverManager.getConnection(myUrl + database, myUser, myPassword); // connect using given database name
			myStatement = myConnection.createStatement();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}