package ticketPackage;

public class Flightline {
	private String id = null, flightline = null, date = null, departure = null, arrival = null, flightCompany = null, plane = null;	
	Flightline(String[] flightline){ //int id, String flightline, String date, String departure, String arrival, String flight_company, String plane
		this.id = flightline[0];
		this.flightline = flightline[1];
		this.date = flightline[2];
		this.departure = flightline[3];
		this.arrival = flightline[4];
		this.flightCompany = flightline[5];
		this.plane = flightline[6];
	}
	public String getId(){
		return this.id;
	}
	public String getFlightline(){
		return this.flightline;
	}
	public String getDate(){
		return this.date;
	}
	public String getDeparture(){
		return this.departure;
	}
	public String getArrival(){
		return this.arrival;
	}
	public String getFlightCompany(){
		return this.flightCompany;
	}
	public String getPlane(){
		return this.plane;
	}
	public String getToString(){
		return this.id + " " + this.flightline + " " + this.date + " " + this.departure + " " + this.arrival + " " + this.flightCompany + " " + this.plane;
	}
}
