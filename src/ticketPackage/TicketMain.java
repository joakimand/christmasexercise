package ticketPackage;
import java.util.Scanner;
public class TicketMain {
	
	static Scanner inputScanner = new Scanner(System.in);
	static TiCreateDB createDB = new TiCreateDB(); // create a new create database object from instance. Create a new database
	static TiUser regularUser = new TiUser();
	static TiAdmin admin = new TiAdmin();
	static TiValidate validate = new TiValidate();
	static SqlQuery sqlQuery = new SqlQuery();
	static String inputString;
	static int inputInt;
	
	// Database info can AND SHOULD be changed in TiCon's constructor and in sqlQuery's constructor
	// Admin account -- bob@customerservice.me -- with password -- Bob --
	
	
	// Search for existing booked flight by customer id AND phone number
	
	public static void main(String[] args) { // main method - start of program
		
		if(!createDB.databaseExist("flight")){
			createDB.databaseConstruct();
		}
		//------------------------- Main Menu - Start */
		do{ // do this
			System.out.println("Main menu\n" + "\n" + "1: Login | " + "2: New user | " + "3: Exit"); // print to screen
			do{
				inputString = inputScanner.next(); // wait for keyboard input from user
				if(!validate.mainMenuValidate(inputString)){ // is it the allowed input?
					System.out.println("Invalid menu option \""+inputString+"\""); // No? Then print to screen
				}
			}
			while(!validate.mainMenuValidate(inputString)); // Was it a correct input? If not, go again!
			inputInt = Integer.parseInt(inputString); // convert the string to an Integer
			switch(inputInt){ // switch!
				case 1:
					 // login
					System.out.println("Email     " + "Password");
					String email = inputScanner.next();
					String password = inputScanner.next();
					do{
						if(!sqlQuery.findUser(email, password)){ // check if user exists
							System.out.println("Invalid email or password!");
							email = inputScanner.next();
							password = inputScanner.next();
						}
						if(sqlQuery.findUser(email, password)){
							if(sqlQuery.findUser(email, password, "rights")){ //check user table for rights == 1
							  	admin.adminMainMenu(admin.getCustomer(email)); // if true - administrator login
							  }
							  else{
							  	// if false - regular user login
								  regularUser.userMenu(regularUser.getCustomer(email));
							  }
						}
					}while(!sqlQuery.findUser(email, password));
					break;
				case 2:
					//register
					regularUser.createCustomer();
					break;
			}	
		}while(!(inputInt == 3)); // as long as you don't press "3" it'll all be fine!
		System.out.println("Please come again!"); // print to screen
		//*/
		//------------------------- Main Menu - End
	}
}
