package ticketPackage;
public class TiAdmin extends TiUser{
	SqlQuery sqlQuery = new SqlQuery();
	TiAdmin(){
		//dataCon("flight");
	}
	public void adminMainMenu(Customer customer){
		System.out.println("Logged in as: Administrator"+ "\n");
		//openConnection();
		do{
			System.out.println("1: Create an airport | " + "2: Create a plane | " + " 3: Create a flight company\n"
					+ "4: Create a flight line | " + "5: Search a flight | " + "6: Book a flight | " + "7: Logout");
			do{
				inputString = inputScanner.next();
				if(!validate.adminMenuValidate(inputString)){
					System.out.println("Invalid menu option \""+inputString+"\""); // No? Then print to screen
				}
			}while(!validate.adminMenuValidate(inputString)); // as long as the input is wrong, repeat!
			inputInt = Integer.parseInt(inputString); // convert your inputString to and Integer and for usage with Switch()
			switch(inputInt){
			case 1:
				// create airport object from instance (String, String) by
				// calling inputData()
				addAirport();
				break;
			case 2:
				// create plane
				addPlane();
				break;
			case 3:
				// create flight company
				addFlightCompany();
				break;
			case 4:
				// create flight line
				addFlightLine();
				break;
			case 5:
				// search flight
				searchFlight();
				break;
			case 6:
				// book flight
				bookFlight(customer);
				break;
			case 7:
				//	closeConnection();
			}
		}while(!(inputInt == 7));		
	}
	private void addAirport(){
		System.out.println("Add an airport using: Name | City | Country");
		String table = "airport";
		String[] column = {"name", "city", "country"};
		String[] value = {inputData(4,inputScanner.next()), inputData(1,inputScanner.next()), inputData(1,inputScanner.next())};
		sqlQuery.insertData(table, column, value);
		
	}
	private void addPlane(){ // 0 airportname 1 regularname 4 planename 5 flightlinenumber 6 flightcompanyname
		System.out.println("Add a plane using: Name | Flight Company");
		String table = "plane";
		String[] column = {"name", "flight_company"};
		String[] value = {inputData(4,inputScanner.next()), inputData(6,inputScanner.next())};
		sqlQuery.insertData(table, column, value);
		
	}
	private void addFlightLine(){
		System.out.println("Add a flight line using: Flightline | Date | Departure | Arrival | Flight Company | Plane");
		String table = "flight_line";
		String[] column = {"flightline", "date", "departure", "arrival", "flight_company", "plane"};
		String[] value = {inputData(5,inputScanner.next()), inputData(9,inputScanner.next()),
				inputData(0,inputScanner.next()), inputData(0,inputScanner.next()), inputData(6,inputScanner.next()), 
				inputData(4,inputScanner.next())};
		sqlQuery.insertData(table, column, value);
	}
	private void addFlightCompany(){
		System.out.println("Add a flight company using: Name");
		String table = "flight_company";
		String[] column = {"name"};
		String[] value = {inputData(6,inputScanner.next())};
		sqlQuery.insertData(table, column, value);
	}
}