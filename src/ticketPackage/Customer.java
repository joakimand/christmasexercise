package ticketPackage;

public class Customer {
	
	private String ssn = null, name = null, surName = null, phone = null, email = null;
	
	Customer(String[] customer){
		this.ssn = customer[0];
		this.name = customer[1];
		this.surName = customer[2];
		this.phone = customer[3];
		this.email = customer[4];
	}
	public String getSSN(){
		return ssn;
	}
	public String getName(){
		return name;
	}
	public String getSurName(){
		return surName;
	}
	public String getPhone(){
		return phone;
	}
	public String getEmail(){
		return email;
	}
	public String[] getCustomer(){
		String[] tempArray = {ssn,name,surName,phone,email};
		return tempArray;
	}
}
