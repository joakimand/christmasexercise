package ticketPackage;
import java.sql.SQLData;
import java.util.Scanner;
public class TiUser extends TiCon{

	protected Scanner inputScanner = new Scanner(System.in);
	protected TiValidate validate = new TiValidate();
	protected SqlQuery sqlQuery = new SqlQuery();
	protected String []aString = {null, null, null, null, null, null};
	protected String inputString = null, query = null;
	private Customer customer;
	String []pattern = {/*0 Airport*/ "[A-Z]{3,3}", /*1 Regular names*/ "[a-zA-Z������]+", /*2 Phone*/ "(0{1,1}[1-9]{1,1}[0-9]{1,2})[0-9]{5,6}", /*3 Email*/ "[a-zA-Z]{1}[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.[a-z]{2,4}",
			/*4 Plane*/ "[a-zA-Z0-9-]+", /*5 Flight Line*/ "[0-9]{,5}", /*6 Flight Company*/ "(?=[A-Z���]).{1,1}(?=[a-z���]).{5,}",	/*7 Social Security Number*/ "[0-9]{4}[-][0-9]{2}[-][0-9]{2}",
			/*8 Password*/ "(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{8,15}", /*9 Digits*/ "\\d+"};
	int inputInt;

	public TiUser(){

	} // add getairport(), getflightcompany() methods
	public void userMenu(Customer customer){
		//System.out.println("Logged in as: User"+ "\n");
		do{
			System.out.println("1: Search flights | " + "2: Book a flight | 3: Print personal information \n"
					+ "4: Print ticket(s) | " + "5: Print all flight companies | " + "6: Print all airports | " + "7: Logout"); // print options
			do{
				inputString = inputScanner.next(); // make a choice
				if(!validate.regularMenuValidate(inputString)){ // is it a valid choice?
					System.out.println("Invalid menu option \""+inputString+"\""); // No? Then print to screen
				}
			}while(!validate.regularMenuValidate(inputString)); // as long as the input is wrong, repeat!
			inputInt = Integer.parseInt(inputString); // convert your inputString to and Integer and for usage with Switch()
			switch(inputInt){ // what choice did we make?
			case 1:
				// search flight
				searchFlight();
				break;
			case 2:
				// book flight
				bookFlight(customer);
				break;
			case 3:
				// print customer info
				System.out.println("---------------\n" + customer.getSSN() + "\n" + customer.getName() + "\n" + customer.getSurName() + "\n" + customer.getPhone() + "\n" + customer.getEmail() + "\n---------------");
				break;
			case 4:
				// print tickets
				getTickets();
				break;
			case 5:
				// print flight companies
				getFlightCompanies();
				break;
			case 6:
				// print airports
				getAirports();
				break;
			}

		}while(!(inputInt == 7)); // Logout
	}
	private void getFlightCompanies(){ // get all info from query and print it
		String[] table = {"flight_company"};
		String[] column = {"*"};
		String[] tempStorage = sqlQuery.retrieveColumn(table, column);
		for(int i = 0; i < tempStorage.length; i++){
			System.out.println(tempStorage[i]);
		}
	}
	private void getAirports(){ // get all info from query and print it
		String[] table = {"airport"};
		String[] column = {"*"};
		String[] tempStorage = sqlQuery.retrieveColumn(table, column);
		int rows = tempStorage.length/4;
		int counter = 0;
		System.out.println("--------------");
		for(int p = 0; p < rows; p++){
			for(int i = 0; i < 4; i++){
				System.out.println(tempStorage[counter]);
				counter++;
			}
			System.out.println("--------------");
			//counter++;
		}
		
	}
	private void getTickets(){
		int numOfColumns = 10; // 10 columns in ticket table
		int counter = 0; // basic counter
		Ticket ticket; // new Ticket object
		Ticket[] ticketArray; // new Ticket[] object (array)
		// variables needed to make a query
		String[] table = {"ticket"};
		String[] column = {"*"};
		// --------------------
		String[] tempStorage; // an String[] to receive all data from query
		tempStorage =  sqlQuery.retrieveColumn(table, column); // take data from query
		int numOfRows = tempStorage.length/numOfColumns; // number of rows from query
		ticketArray = new Ticket[numOfRows]; // set size to number of rows
		for(int p = 0; p < ticketArray.length; p++){ // run as long as there are more rows
			String[] tempArray = new String[numOfColumns]; // create a temporary holder for data extraction from the master query array
			for(int i = 0; i < tempArray.length; i++){ // run as many times as there are containers
				tempArray[i] = tempStorage[counter]; // add data from master array
				if(i == tempArray.length-1){ // if we meet the max number
					ticketArray[p] = ticket = new Ticket(tempArray); // create a new Ticket and add to master Ticket array
					System.out.println(ticketArray[p].getToString()); // print what we just added
				}
				counter++;
			}
		}
	}
	//-------------------------------------------------------
	protected void searchFlight(){
		String[] aString = null, tempString = null;
		String[] anArray;
		do{
			System.out.println("Search flight by: | 1. Arrival | 2. Departure"); // add menu validation
			switch(Integer.parseInt(inputData(9,inputScanner.next()))){
			case 1:
				tempString = new String[] {"arrival"};
				System.out.println("Where do you want to fly? 3 capital letters");
				aString = new String[] {inputData(0,inputScanner.next())};
				break;
			case 2:
				tempString = new String[] {"departure"};
				System.out.println("Where do you want to fly from? 3 capital letters");
				aString = new String[] {inputData(0,inputScanner.next())};
				break;
			}
			String[] column = {"*"};
			String[] table = {"flight_line"};
			anArray = sqlQuery.retrieveColumn(table, column, tempString, aString);
			int rows = anArray.length/7;
			int counter = 0;
			System.out.println("--------------");
			for(int p = 0; p < rows; p++){
				for(int i = 0; i < 7 ; i++){
					System.out.println(anArray[counter]);
					counter++;
				}
				System.out.println("--------------");
			}
			if(anArray.length == 0){
				System.out.println("Flight doesn't exist");
			}
		}while(anArray.length == 0);
	}
	//-------------------------------------------------------
	private String[] selectFlight(){
		searchFlight();
		System.out.println("Select flight id, the first number in each block");
		String[] inputString = {inputData(9,inputScanner.next())};
		String[] column = {"*"};
		String[] table = {"flight_line"};
		String[] where = {"id"};
		String[] anArray = sqlQuery.retrieveColumn(table, column, where, inputString);
		for(int i = 0; i < anArray.length; i++){
			System.out.println(anArray[i]);
		}
		return anArray;
	}
	//-------------------------------------------------------
	protected void bookFlight(Customer customer){
		String[] aStringArray = selectFlight();
		Flightline flight = new Flightline(aStringArray);
		String[] table = {"booking"};
		String[] column = {"booking_id"};
		String[] anArray = {customer.getSSN(),flight.getFlightline()};
		String[] whereValue = {"customer_id","flightline"};
		System.out.println("Confirm? (Y/WHATEVER)");
		inputString = inputScanner.next();
		if(sqlQuery.retrieveColumn(table, column, whereValue, anArray).length > 1){
			System.out.println("A booking on this flight already exists!");
		}
		else
		if(inputString.contains("Y") || inputString.contains("y")){
			sqlQuery.insertData("booking", whereValue, anArray);
			createTicket(customer, flight);
			System.out.println("Complete!");
		}
	}
	private void createTicket(Customer customer, Flightline flight){
		// Select booking_id
		String tempSelect = "(SELECT booking_id FROM booking WHERE customer_id LIKE '"+customer.getSSN()+"' AND flightline LIKE '"+flight.getFlightline()+"')";
		//--------------------
		String[] column = {"booking_id", "ssn", "name", "surname", "arrival", "departure", "flight_company", "plane", "date"};
		String[] aStringArray = {tempSelect, customer.getSSN(), customer.getName(), customer.getSurName(), flight.getArrival(), flight.getDeparture(),
				flight.getFlightCompany(), flight.getPlane(), flight.getDate()};
		for(int i = 0; i < aStringArray.length; i++){
			System.out.println(aStringArray[i]);
		}
		// save to database
		sqlQuery.insertData("ticket", column, aStringArray);
	}
	//-------------------------------------------------------
	public void createCustomer(){
		System.out.println("New customer\n" + "Please input: ");
		//---- Enter values
		System.out.println("Social Security Number");
		inputString = inputScanner.nextLine();
		aString[0] = inputData(7, inputString, "ssn", "customer");

		System.out.println("Name");
		inputString = inputScanner.nextLine();
		aString[1] = inputData(1, inputString);

		System.out.println("Surname");
		inputString = inputScanner.nextLine();
		aString[2] = inputData(1, inputString);

		System.out.println("Phone number");
		inputString = inputScanner.nextLine();
		aString[3] = inputData(2, inputString);

		System.out.println("Email");
		inputString = inputScanner.nextLine();
		aString[4] = inputData(3, inputString);
		//---- //
		customer = new Customer(aString);
		boolean tempBool = false;
		if(!tempBool){
			String[] tempColumns = {"ssn","name","sur_name","phone","email"};
			sqlQuery.insertData("customer", tempColumns, aString);
		}
		System.out.println("Enter a password");
		inputString = inputScanner.next();
		String[] tempString = {customer.getEmail(),inputData(8, inputString)};
		String[] tempColumns = {"email","password"};
		sqlQuery.insertData("user", tempColumns, tempString);
		System.out.println("User creation complete!");
		//createUser(customer.getEmail(), aString[5]);
	}
	//------------------------------------------------------------------------------------- GetCustomer
	public Customer getCustomer(String email){
		connectToDatabase("flight");
		String[] column = {"*"};
		String[] table = {"customer"};
		String[] where = {"email"};
		String[] data = {email};
		String[] tempArray = sqlQuery.retrieveColumn(table, column, where, data);
		/*for(int i = 0; i < tempArray.length; i++){
			System.out.println(tempArray[i]);
		}*/
		customer = new Customer(tempArray);
		return customer;
	}		
	//------------------------------------------------------------------------------------- InputData // add to validate instead
	public String inputData(int pat, String input){ // validate
		do{
			if(!validate.validate(input, pattern[pat])){
				System.out.println("Invalid input! ");
				input = inputScanner.next();
				if(input.contains(" "))
				input += inputScanner.nextLine();
			}
		}while(!validate.validate(input, pattern[pat]));
		return input;
	}
	public String inputData(int pat, String input, String column, String table){ // validate for database
		do{
			query = "SELECT `"+column+"` FROM `flight`.`"+table+"` WHERE `"+column+"` LIKE \""+input+"\";";
			if(!validate.validate(input, pattern[pat]) || validate.sqlValidate(query)){
				if(!validate.validate(input, pattern[pat])){
					System.out.println("Invalid input");
				}
				if(validate.sqlValidate(query)){
					System.out.println("Input already exists in database");
				}
				input = inputScanner.next();
				if(input.contains(" "))
				input += inputScanner.nextLine();
			}
		}while(!validate.validate(input, pattern[pat]) || validate.sqlValidate(query));
		return input;
	}
}